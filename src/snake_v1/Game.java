package snake_v1;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.Timer;

//import sun.nio.ch.AbstractPollArrayWrapper;

public class Game extends JComponent implements Runnable {

	private int gameWidth;
	private int gameHeight;
	private int delay = 1000;
	private int cellSize = 15;
	
	private boolean running = false;
	private int scores = 0;
	private long startTime, currentTime;
	private Dircetions dircetions = new Dircetions();
	private int headX = gameWidth / 2;
	private int headY = gameHeight / 2;
	private int size = 1;
	
	private SnakeController controller = new SnakeController();
	private Thread gameThread;
	private Random generator = new Random();
	private Timer updateTimer;
	
	private JLabel scoreLabel = new JLabel("Scores: " + scores);
	private JLabel timeLabel = new JLabel("Time: " + currentTime / 1000 + "s");
	
	private ArrayList<SnakeCell> snakeBody = new ArrayList<SnakeCell>();
	private AppleCell apple = null;
	
	private String msg = "";
	
	
	public Game() {
				
		addKeyListener(controller);
		setFocusable(true);
		
		setBackground(Color.BLACK);
				
		startTime = System.currentTimeMillis();
		
		updateTimer = new Timer(delay, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				checkStatus();
				repaint();
			}
		});
		
		this.start();
	}
	
	public void start() {
		
		this.running = true;
		
		this.gameThread = new Thread(this);
		this.gameThread.setDaemon(true);
		this.gameThread.start();
	}
	
	public void stop() {
		
		this.running = false;		
		repaint();
		
		try {
			this.gameThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		updateTimer.start();
	}			
	
	public void setSpeed(int speed) {
		updateTimer.setDelay(delay / speed);
	}

	private void checkStatus() {
			
		gameHeight = getHeight();
		gameWidth = getWidth();
			
		if (snakeBody.size() == 0) { 
			
			headX = gameWidth / 2;
			headY = gameHeight / 2;
			snakeBody.add(new SnakeCell(headX, headY, cellSize));
		}

		if(apple == null) {
			
			int randX = generator.nextInt(gameWidth - cellSize);
			int randY = generator.nextInt(gameHeight - cellSize);
			apple = new AppleCell(randX, randY, cellSize);
		}			
	
		if (headX > apple.x-cellSize && headX<apple.x+cellSize &&
				headY > apple.y-cellSize && headY<apple.y+cellSize) {
			apple=null;
			size++;
			scores++;
		}
			
		int i=0;
		for (SnakeCell cell: snakeBody) { 
			if (headX == cell.x && headY == cell.y) 
				if (i != snakeBody.size() - 1)  
					stop(); 
			i++;
		}
			
		if (headX < 0 || headX > gameWidth-cellSize || headY < 0 || headY > gameHeight-cellSize) 
			stop();
			
		if (dircetions.right) 
			headX=headX+cellSize;		
			
		if (dircetions.left) 
			headX=headX-cellSize;
			
		if (dircetions.up) 
			headY=headY-cellSize;
			
		if (dircetions.down) 
			headY=headY+cellSize;
			
		snakeBody.add(new SnakeCell(headX, headY, cellSize));
			
		if (snakeBody.size() > size) 
			snakeBody.remove(0);

		if(running) {
			
			currentTime = System.currentTimeMillis() - startTime;
				
			scoreLabel.setFont(new Font("Serif", Font.PLAIN, gameHeight/30));
			timeLabel.setFont(new Font("Serif", Font.PLAIN, gameHeight/30));
				
			scoreLabel.setText("Scores: " + scores + msg);
			
			if(currentTime / 1000 > 60) {
				timeLabel.setText("Time: " + currentTime / 60000 + "m " + 
						(currentTime % 60000 ) / 1000 + "s");
			}else {
				timeLabel.setText("Time: " + currentTime / 1000 + "s");
			}
			
			cellSize = gameHeight / 50;
		}

		requestFocusInWindow();
	}
	
	@Override
	protected void paintComponent(Graphics g){
		
		Graphics2D g2 = (Graphics2D)g;
		
		scoreLabel.setBounds(gameWidth / 4 * 3, gameHeight / 10 * 9, gameWidth / 9, gameHeight/30);
		timeLabel.setBounds(gameWidth / 10, gameHeight / 10 * 9, gameWidth / 9, gameHeight/30);

		add(scoreLabel);
		add(timeLabel);
		
		if(running){
			
			if(snakeBody.size() != 0) {
				for(SnakeCell cell: snakeBody) {
					cell.draw(g2);
				}
			}
			
			if(apple != null) {
				apple.draw(g2);
			}
			
		}else {
			
			g.setFont(new Font("Serif", Font.PLAIN, gameHeight/30));
			g.drawString("GAME OVER", gameWidth / 2, gameHeight / 2);
		}
	}

	private class Dircetions{
		public boolean left = false, right = false, up = false, down = false;
	}
	
	private class SnakeController implements KeyListener{
		
		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			
			if(keyCode == KeyEvent.VK_LEFT && !dircetions.right) {
				dircetions.left = true;
				dircetions.up = false;
				dircetions.down = false;
			}
			else if (keyCode == KeyEvent.VK_RIGHT && !dircetions.left) {
				dircetions.right = true;
				dircetions.up = false;
				dircetions.down = false;
			}
			else if (keyCode == KeyEvent.VK_UP && !dircetions.down) {
				dircetions.up = true;
				dircetions.left = false;
				dircetions.right = false;
			}
			else if (keyCode == KeyEvent.VK_DOWN && !dircetions.up) {
				dircetions.down = true;
				dircetions.left = false;
				dircetions.right = false;
			}
		}
		
		@Override
		public void keyReleased(KeyEvent e) {}
 
		@Override
		public void keyTyped(KeyEvent e) {}
	}
}
