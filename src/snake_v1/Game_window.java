package snake_v1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.Console;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Panel;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javafx.scene.layout.BorderPane;
import java.awt.Font;
import javax.swing.JSlider;
import javax.swing.JLabel;

public class Game_window {

	private JFrame frmSnake;
	private Game game = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game_window window = new Game_window();
					window.frmSnake.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Game_window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSnake = new JFrame();
		frmSnake.setTitle("Snake");
		frmSnake.setBounds(100, 100, 1064, 761);
		frmSnake.setFocusable(true);
		frmSnake.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton NewGameButton = new JButton("New Game");
		NewGameButton.setFont(new Font("Tahoma", Font.PLAIN, 32));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(169, 169, 169));
		panel.setLayout(new BorderLayout());
		
		JSlider speedSlider = new JSlider();
		speedSlider.setMinimum(1);
		speedSlider.setValue(1);
		
		JLabel lblNewLabel = new JLabel("Adjust game speed");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 32));
		
		GroupLayout groupLayout = new GroupLayout(frmSnake.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1122, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(NewGameButton, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE)
							.addGap(54)
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(speedSlider, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(NewGameButton, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(38)
							.addComponent(speedSlider, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		
		NewGameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if(game != null) {
					panel.remove(game);
					panel.revalidate();
				}
				
				game = new Game();
				panel.add(game, BorderLayout.CENTER);
				panel.revalidate();
				
				game.setSpeed(speedSlider.getValue());
				game.setSize(panel.getSize());
			}
		});
		
		speedSlider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				
				if(game != null) {
					 game.setSpeed(speedSlider.getValue());
				}
			}
		});
		
		panel.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				if(game != null) {
					game.setSize(panel.getSize());
				}
				
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		frmSnake.getContentPane().setLayout(groupLayout);
	}
}
