package snake_v1;

import java.awt.Color;
import java.awt.Graphics;

public class AppleCell {
	
	public int x, y, size;
	
	public AppleCell(int x, int y, int size) {
		this.x = x;
		this.y = y;
		this.size = size;
	}
	
	public void draw(Graphics g){
		g.setColor(Color.RED);
		g.fillRect(x, y, size, size);
	}
}
