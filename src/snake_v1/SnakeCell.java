package snake_v1;

import java.awt.Color;
import java.awt.Graphics;

public class SnakeCell {
	
	public int x, y, size;
	
	public SnakeCell(int x, int y, int size) {
		this.x = x;
		this.y = y;
		this.size = size;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(x, y, size, size);
	}
}
